import { useState, useEffect } from "react";
import * as Location from "expo-location";

export default function useSearch() {
  const [cepLoad, setCEPLoad] = useState(""),
    [coordinate, setCoordinate] =
      useState<{
        latitude: number;
        longitude: number;
      }>();

  useEffect(() => {
    (async () => {
      try {
        const gpsGranted = await getPermission();
        if (gpsGranted) {
          setCoordinate(await getCoordinate());
        }
      } catch (error) {}
    })();
  }, []);

  useEffect(() => {
    (async () => {
      try {
        if (coordinate) {
          setCEPLoad(await getCEP());
        }
      } catch (error) {}
    })();
  }, [coordinate]);

  async function getPermission(): Promise<boolean> {
    try {
      const { status } = await Location.requestForegroundPermissionsAsync();
      return status === "granted";
    } catch (error) {
      return false;
    }
  }

  async function getCoordinate(): Promise<{
    latitude: number;
    longitude: number;
  }> {
    const location = await Location.getCurrentPositionAsync({
      accuracy: Location.Accuracy.Highest,
    });

    return location.coords;
  }

  async function getCEP(): Promise<string> {
    if (coordinate) {
      const address = await Location.reverseGeocodeAsync(coordinate);
      if (address.length > 0) {
        return address[0].postalCode || "";
      }
    }
    return "";
  }

  return {
    cepLoad,
  };
}
