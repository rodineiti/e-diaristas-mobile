import { useState, useMemo } from "react";
import { UserShortInterface } from "data/types/UserInterface";
import { ValidationService } from "data/services/ValidationService";
import { ApiService } from "data/services/ApiService";

export default function useIndex() {
  const [cep, setCEP] = useState(""),
    cepValid = useMemo(() => {
      return ValidationService.cep(cep);
    }, [cep]),
    [error, setError] = useState(""),
    [search, setSearch] = useState(false),
    [loading, setLoading] = useState(false),
    [list, setList] = useState([] as UserShortInterface[]),
    [listRest, setListRest] = useState(0);

  async function getProfessions(cep: string) {
    try {
      setSearch(false);
      setLoading(true);
      setError("");
      const { data } = await ApiService.get<{
        diaristas: UserShortInterface[];
        quantidade_diaristas: number;
      }>(`/api/diaristas-cidade?cep=${cep.replace(/\D/g, "")}`);
      setSearch(true);
      setLoading(false);
      setList(data.diaristas);
      setListRest(data.quantidade_diaristas);
    } catch (error) {
      console.error(error);
      setLoading(false);
      setError("CEP não encontrado");
    }
  }

  return {
    cep,
    setCEP,
    cepValid,
    getProfessions,
    error,
    list,
    listRest,
    search,
    loading,
  };
}
