import React from "react";
import { View } from "react-native";
import { StackNavigationProp } from "@react-navigation/stack";
import { RootStackParamList } from "ui/router/Router";
import ButtonCustom from "ui/components/inputs/ButtonCustom";

type NavigationProp = StackNavigationProp<RootStackParamList, "Index">;

interface IndexProps {
  navigation: NavigationProp;
}

const Index: React.FC<IndexProps> = ({ navigation }) => {
  return (
    <View style={{ flex: 1, justifyContent: "center" }}>
      <ButtonCustom
        mode="contained"
        onPress={() => navigation.navigate("Search")}
      >
        Encontrar Diaristas
      </ButtonCustom>
    </View>
  );
};

export default Index;
