import React, { useEffect } from "react";
import { useTheme } from "@emotion/react";
import styled from "@emotion/native";
import { ScrollView } from "react-native";
import PageTitle from "ui/components/data-display/PageTitle";
import UserInfo from "ui/components/data-display/UserInfo";
import InputTextCustom from "ui/components/inputs/InputTextCustom";
import ButtonCustom from "ui/components/inputs/ButtonCustom";
import { TextInputMask } from "react-native-masked-text";
import useIndex from "data/hooks/pages/useIndex";
import useSearch from "data/hooks/pages/useSearch";

const FormContainer = styled.View`
  padding: 0 ${({ theme }) => theme.spacing(2)};
`;

const TextContainer = styled.Text`
  text-align: center;
  padding: ${({ theme }) => theme.spacing(4) + " " + theme.spacing()};
`;

const Error = styled(TextContainer)`
  color: ${({ theme }) => theme.colors.error};
`;

const Response = styled.View`
  padding: ${({ theme }) => theme.spacing(0) + " 0 " + theme.spacing(8)};
`;

const Search: React.FC = () => {
  const { colors } = useTheme();
  const {
      cep,
      setCEP,
      cepValid,
      getProfessions,
      error,
      list,
      listRest,
      search,
      loading,
    } = useIndex(),
    { cepLoad } = useSearch();

  useEffect(() => {
    if (cepLoad && !cep) {
      setCEP(cepLoad);
      getProfessions(cepLoad);
    }
  }, [cepLoad]);

  return (
    <ScrollView>
      <PageTitle
        title="Conheça os profissionais"
        subtitle="Preencha seu endereço e veja todos os profissionais da sua região."
      />

      <FormContainer>
        <TextInputMask
          type="custom"
          value={cep}
          options={{ mask: "99.999-999" }}
          customTextInput={InputTextCustom}
          customTextInputProps={{ label: "Digite seu CEP" }}
          onChangeText={(text) => setCEP(text)}
        />

        {error ? <Error>{error}</Error> : null}

        <ButtonCustom
          mode="contained"
          style={{ marginTop: 32 }}
          color={colors.accent}
          disabled={!cepValid || loading}
          onPress={() => getProfessions(cep)}
          loading={loading}
        >
          Buscar
        </ButtonCustom>
      </FormContainer>

      {search && list.length > 0 ? (
        <Response>
          {list.map((item, index) => (
            <UserInfo
              key={String(index)}
              name={item.nome_completo}
              rating={item.reputacao || 0}
              picture={item.foto_usuario || ""}
              description={item.cidade}
              darker={index % 2 === 1}
            />
          ))}

          {listRest > 0 ? (
            <TextContainer>
              ...e mais {listRest}{" "}
              {listRest > 1 ? "profissionais atendem" : "profissional atende"}{" "}
              ao seu endereço.
            </TextContainer>
          ) : null}

          <ButtonCustom mode="contained" color={colors.accent}>
            Contratar um profissional
          </ButtonCustom>
        </Response>
      ) : (
        <TextContainer>
          Ainda não temos nenhuma diarista em sua região.
        </TextContainer>
      )}
    </ScrollView>
  );
};

export default Search;
