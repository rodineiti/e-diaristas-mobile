import React from "react";
import styled from "@emotion/native";

const Container = styled.View`
  margin: ${({ theme }) => theme.spacing(5) + " " + 0};
`;

const Title = styled.Text`
  margin: 0;
  color: ${({ theme }) => theme.colors.primary};
  font-size: 16px;
  text-align: center;
  font-weight: bold;
`;

const SubTitle = styled.Text`
  margin: ${({ theme }) => theme.spacing(1.5) + " auto"};
  color: ${({ theme }) => theme.colors.text};
  font-size: 14px;
  text-align: center;
  font-weight: normal;
  max-width: 275px;
`;

export interface PageTitleProps {
  title: string;
  subtitle: string | JSX.Element;
}

const PageTitle: React.FC<PageTitleProps> = ({ title, subtitle }) => {
  return (
    <Container>
      <Title>{title}</Title>
      <SubTitle>{subtitle}</SubTitle>
    </Container>
  );
};

export default PageTitle;
