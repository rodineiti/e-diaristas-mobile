import React from "react";
import { AirbnbRating } from "react-native-ratings";
import { View } from "react-native";
import { Avatar } from "react-native-paper";
import styled, { css } from "@emotion/native";
import AppTheme from "ui/themes/app.theme";

const Container = styled(View, {
  shouldForwardProp: (prop) => prop !== "darker",
})<{ darker: boolean }>`
  flex-flow: row;
  align-items: center;
  padding: ${({ theme }) => theme.spacing(3) + " " + theme.spacing(2)};
  background-color: ${({ theme, darker }) =>
    theme.colors.grey[darker ? 100 : 50]};
`;

const ContainerInfo = styled.View`
  flex: 1;
  padding-left: ${({ theme }) => theme.spacing(2)};
`;

const Name = styled.Text`
  color: ${({ theme }) => theme.colors.text};
  margin-bottom: ${({ theme }) => theme.spacing(0.5)};
  font-weight: bold; ;
`;

const Description = styled.Text`
  color: ${({ theme }) => theme.colors.text};
  margin-top: ${({ theme }) => theme.spacing(0.5)};
`;

const Rating = styled(AirbnbRating)``;
Rating.defaultProps = {
  isDisabled: true,
  showRating: false,
  size: 10,
  count: 5,
  selectedColor: AppTheme.colors.warning,
  // @ts-ignore
  starContainerStyle: css`
    width: 100%;
    justify-content: flex-start;
  `,
};

export interface UserInfoProps {
  picture: string;
  name: string;
  rating: number;
  description?: string;
  darker?: boolean;
}

const UserInfo: React.FC<UserInfoProps> = ({
  picture,
  name,
  rating,
  description,
  darker,
}) => {
  return (
    <Container darker={Boolean(darker)}>
      <Avatar.Image source={{ uri: picture }} />
      <ContainerInfo>
        <Name>{name}</Name>
        <Rating defaultRating={rating} />
        <Description>{description}</Description>
      </ContainerInfo>
    </Container>
  );
};

export default UserInfo;
