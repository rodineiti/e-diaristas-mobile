import { TextInput } from "react-native-paper";
import styled from "@emotion/native";

const InputTextCustom = styled(TextInput)``;

InputTextCustom.defaultProps = {
  mode: "outlined",
};

export default InputTextCustom;
