import { Button } from "react-native-paper";
import styled from "@emotion/native";

const ButtonCustom = styled(Button)`
  margin: 0px auto;
  padding: ${({ theme }) => theme.spacing(0.5)};
  width: 100%;
  max-width: 300px;
  border-radius: ${({ theme }) => theme.shape.borderRadius};
`;

ButtonCustom.defaultProps = {
  dark: true,
  uppercase: false,
};

export default ButtonCustom;
